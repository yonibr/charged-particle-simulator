package chargedParticleSimulator;

/**
 * Adds functionality to {@link RateTracker} allowing multiple sources to partially modify rate. For
 * example, this means that if two threads are working on the same task, and one takes exactly twice
 * the amount of time compared to the other, the rate returned by getFPS() will be
 * 3*FASTER_THREAD_TIME/2
 * 
 * @author Yoni Rubenstein
 */
public class MultiSourceRateTracker extends RateTracker
{
    private final int sources;

    /**
     * Instantiates a new MultiSourceRateTracker.
     *
     * @param sources
     *            the number of sources dividing the work
     */
    public MultiSourceRateTracker(int sources)
    {
        super();
        this.sources = sources;
    }

    @Override
    public int getRPS()
    {
        return rps / sources;
    }
}
