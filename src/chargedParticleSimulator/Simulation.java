package chargedParticleSimulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Random;

public class Simulation
{
    // Constants
    private static final float     MAX_CHARGE             = .001f;
    static final int               DEFAULT_PARTICLE_COUNT = 120;
    volatile Rectangle             bounds;

    // Instance variables
    public final ChargedParticle[] particles;                     // = Collections.synchronizedList(new
                                                                  // ArrayList<ChargedParticle>());
    // public final List<ChargedParticle> particles = new ArrayList<ChargedParticle>();

    public Simulation(int numParticles, Rectangle bounds)
    {
        particles = new ChargedParticle[numParticles];
        this.bounds = bounds;
        Random random = new Random();
        // Creates particles and adds them to the list
        for (int i = 0; i < numParticles; i++)
        {
            int x = random.nextInt(bounds.width - 100) + 2 * bounds.x;
            int y = random.nextInt(bounds.height - 100) + 2 * bounds.y;
            float charge = (random.nextFloat() - .5f) * MAX_CHARGE * 2;

            particles[i] = new ChargedParticle(x, y, charge);
        }
    }

    /**
     * Draws each of the particles (not including the field around the particles).
     *
     * @param g2d
     *            {@link Graphics2D} object cast from Driver's {@link Graphics} context
     */
    public void drawParticles(Graphics2D g2d)
    {
        for (ChargedParticle particle : particles)
        {
            Color color;

            if (particle.charge == 0)
                color = Color.GRAY;
            else
                color = new Color(255, 255, 255);

            g2d.setColor(color);
            g2d.fillOval((int) (particle.x - particle.radius), (int) (particle.y - particle.radius),
                    particle.radius * 2, particle.radius * 2);
        }
    }

    /**
     * Calculates particle motion.
     *
     * Could be optimized slightly by sharing acceleration data between particles, but there's not much
     * point since this isn't a bottleneck and readability would be sacrificed.
     */
    public void calculate()
    {
        for (ChargedParticle particle1 : particles)
        {
            for (ChargedParticle particle2 : particles)
            {
                if (particle1 != particle2)
                {
                    particle1.setAccelerationVector(particle2);
                    particle1.accelerate(SingletonVector2D.get().x, SingletonVector2D.get().y);
                }
            }
            handleCollision(particle1);
        }
        Arrays.stream(particles).forEach(ChargedParticle::move);
    }

    /**
     * Check for collisions with the wall, and bounce the particle if necessary.
     *
     * @param particle
     *            {@link ChargedParticle} to check for/handle collisions
     */
    private void handleCollision(ChargedParticle particle)
    {
        if (particle.x < bounds.x + 10 + particle.radius)
        {
            particle.x = bounds.x + 10 + particle.radius;
            bounce(1, 0, particle);
        }
        else if (particle.x > bounds.x + bounds.width - 10 - particle.radius)
        {
            particle.x = bounds.x + bounds.width - 10 - particle.radius;
            bounce(1, 0, particle);
        }
        if (particle.y < bounds.y + 10 + particle.radius)
        {
            particle.y = bounds.y + 10 + particle.radius;
            bounce(0, 1, particle);
        }
        else if (particle.y > bounds.y + bounds.height - 10 - particle.radius)
        {
            particle.y = bounds.y + bounds.height - 10 - particle.radius;
            bounce(0, 1, particle);
        }
    }

    /**
     * Bounce a particle off of a wall.
     *
     * @param normal
     *            {@link Vector2D} representing the normal to the wall
     * @param particle
     *            {@link ChargedParticle} to bounce
     */
    private void bounce(float x, float y, ChargedParticle particle)
    {
        float d = (x * particle.dx + y * particle.dy) * 2;
        particle.dx = particle.dx - d * x;
        particle.dy = particle.dy - d * y;
    }

}
