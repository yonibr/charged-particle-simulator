package chargedParticleSimulator;

public class FastMath
{

    /**
     * Calculates fast inverse square root, a la Quake III Arena.
     * 
     * @param number
     *            {@code float} to take the inverse square root of
     * @return inverse square root of number
     */
    public static float fastInvSqrt(float number)
    {
        float xhalf = 0.5f * number;
        int i = Float.floatToIntBits(number);
        i = 0x5f3759df - (i >> 1);
        number = Float.intBitsToFloat(i);
        number = number * (1.5f - xhalf * number * number);
        return number;
    }

    public static float distanceSquared(float x1, float y1, float x2, float y2)
    {
        x1 -= x2;
        y1 -= y2;
        return x1 * x1 + y1 * y1;
    }

}
