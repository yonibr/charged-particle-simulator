package chargedParticleSimulator;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

@SuppressWarnings("serial")
class SimulationSettingsPanel extends JPanel
{
    private Driver  parentDriver;
    private JSlider threadCountSlider, particleCountSlider;
    private JLabel  threadLabel = new JLabel("Threads:  " + Driver.DEFAULT_IMAGE_THREADS),
            particleLabel = new JLabel("Number of particles:  " + Simulation.DEFAULT_PARTICLE_COUNT);

    public SimulationSettingsPanel(Driver driver)
    {
        parentDriver = driver;
        Dimension preferredSize = new Dimension(750, 40);
        setSize(preferredSize);
        setMinimumSize(preferredSize);
        setPreferredSize(preferredSize);
        threadCountSlider = new JSlider(1, 24, Driver.DEFAULT_IMAGE_THREADS);
        threadCountSlider.setMajorTickSpacing(4);
        threadCountSlider.setMinorTickSpacing(1);

        threadCountSlider.setSnapToTicks(true);
        threadCountSlider.setPaintTicks(true);
        threadCountSlider.addChangeListener(e -> setThreads(((JSlider) e.getSource()).getValue()));
        add(threadCountSlider);
        particleCountSlider = new JSlider(1, 250, Simulation.DEFAULT_PARTICLE_COUNT);
        particleCountSlider.setMajorTickSpacing(50);
        particleCountSlider.setMinorTickSpacing(10);
        particleCountSlider.setPaintTicks(true);
        particleCountSlider.addChangeListener(e -> setParticles(((JSlider) e.getSource()).getValue()));
        add(particleCountSlider);

        add(threadLabel);
        add(particleLabel);
    }

    private void setThreads(int n)
    {
        threadLabel.setText("Threads:  " + n);
        parentDriver.threads = n;
        parentDriver.rebuildImage();
    }

    private void setParticles(int n)
    {
        particleLabel.setText("Number of particles:  " + n);
        parentDriver.newSimulation(n);
    }
}
