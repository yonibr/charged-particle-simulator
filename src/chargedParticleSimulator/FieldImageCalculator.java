package chargedParticleSimulator;

/**
 * FieldImageCalculator is a class implementing PixelValueCalculator, and its purpose is to
 * encapsulate a method for calculating pixel data with respect to the value of the electric field
 * strength at each coordinate.
 * 
 * @author Yoni Rubenstein
 */
public final class FieldImageCalculator implements PixelValueCalculator
{
    /**
     * @param particles
     *            {@link ChargedParticle} list to determine fields for a given pixel
     * @param start
     *            index of first pixel to operate on
     * @param width
     *            pixel width of segment of data to operate on, which is not necessarily the "actual"
     *            width of the pixel array.
     * @param height
     *            pixel height of segment of data to operate on, which is not necessarily the "actual"
     *            height of the pixel array.
     * @return the pixel data
     */
    private int[] getFieldImageSection(ChargedParticle[] particles, int start, int width,
            int height)
    {
        int[] mem = new int[width * height];
        // Starting place in the array
        int xStart = 0;
        int yStart = start / width;
        int yStop = yStart + height;
        int xStop = xStart + width;

        int[] color = { 0, 0, 0 }; // Black
        
        int numParticles = particles.length;

        // Iterate over the array, using a nested for loop to simplify method calls and readability.
        // The nesting order doesn't seem to affect speed on systems used for testing. 
        for (int x = xStart; x < xStop; x++)
        	for (int y = yStart; y < yStop; y++)
            {
                color[0] = color[1] = color[2] = 0;
                float cumulativeFieldStrength = 0;

                // Iterate through particles and determine their impact in
                // electric field at the given point, and add that to
                // cumulativeFieldStrength.
                for (int i = 0; i < numParticles; i++)
                    cumulativeFieldStrength += particles[i].electricFieldStrengthAtPoint(x, y);

                float signum = Math.signum(cumulativeFieldStrength);
                float magnitude = cumulativeFieldStrength * signum;
                // Use Square root to make the fields look better/smaller
                cumulativeFieldStrength = signum * (magnitude = (float) Math.sqrt(magnitude) * 2f);

                // Clamp cumulativeFieldStrength to ± 255
                if (magnitude > 255)
                    cumulativeFieldStrength = signum * (magnitude = 255);

                // If the field is positive (or zero, though that won't be visible), this sets the red
                // component of color to the field strength. Otherwise, the same thing is done but to
                // the blue component, and the negative is dropped.
                color[(int) signum + 1] = (int) magnitude;

                // Composes the color and sets the relevant pixel's value
                int argb = color[0] | (color[1] << 8) | (color[2] << 16) | (0xff << 24);
                mem[x + (y - yStart) * width] = argb;
            }

        return mem;
    }

    @Override
    public int[] calculate(Object information, int start, int width, int height)
    {
        return getFieldImageSection((ChargedParticle[]) information, start, width, height);
    }
}
