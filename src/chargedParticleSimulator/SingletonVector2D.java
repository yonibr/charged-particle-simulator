package chargedParticleSimulator;

/**
 * A Vector2D implementation that acts like a register for fast access and no object creation
 *
 *
 * @author Yoni Rubenstein
 */
public class SingletonVector2D
{

    private static ThreadLocal<SingletonVector2D> instance = ThreadLocal.withInitial(SingletonVector2D::new);
    // Elements
    public float                                  x        = 0, y = 0;

    // This class should never be instantiated
    private SingletonVector2D()
    {
    }

    public static SingletonVector2D get()
    {
        return instance.get();
    }

    public void scalarMultiple(float scalar)
    {
        x *= scalar;
        y *= scalar;
    }

    public void add(float x, float y)
    {
        this.x += x;
        this.y += y;
    }

    /**
     * Normalizes a Vector2D in a fast way.
     *
     * @return normalized Vector2D
     */
    public void fastNormalize()
    {
        scalarMultiple(FastMath.fastInvSqrt(getMagnitudeSquared()));
    }

    /**
     * Calculates dot product of stored vector with parameters x, y
     *
     */
    public float dot(float x, float y)
    {
        return this.x * x + this.y * y;
    }

    /**
     * Returns magnitude of stored vector
     *
     */
    public float getMagnitude()
    {
        return (float) Math.hypot(x, y);
    }

    /**
     * Gets the square of the magnitude of the stored vector. Significantly faster than squaring
     * {@link SingletonVector2D#getMagnitude()}, so use this method when possible.
     *
     */
    public float getMagnitudeSquared()
    {
        return x * x + y * y;
    }

    public String getString()
    {
        return "(" + x + ", " + y + ")";
    }
}
