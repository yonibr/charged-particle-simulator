package chargedParticleSimulator;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * This class enables multithreaded updating of an image using a defined
 * {@link PixelValueCalculator}. Note that when retrieving pixel values, the current pixel data is
 * returned, rather than the image data after all queued updates. Perhaps a future implementation
 * will add a feature where it is possible to wait for all parts of the image to be updated.
 * 
 * @author Yoni Rubenstein
 */
public class MultithreadedImage
{
    private final PixelValueCalculator   calculator;
    private final SubImage[]             sections;
    private final MultiSourceRateTracker rateTracker;
    int[]                                pixels;

    /**
     * Instantiates a new MultithreadedImage.
     * 
     * @param width
     *            width (in pixels) of the image to create
     * @param height
     *            height (in pixels) of the image to create
     * @param threads
     *            number of threads to use for updating the image
     * @param calculator
     *            a class implementing {@link PixelValueCalculator} to be used for updating the image
     */
    public MultithreadedImage(int width, int height, int threads, PixelValueCalculator calculator)
    {
        this.calculator = calculator;
        rateTracker = new MultiSourceRateTracker(threads);
        pixels = new int[width * height];
        this.sections = new SubImage[threads];

        int extraPix = height % threads;
        int start = 0;
        for (int i = 0; i < threads; i++)
        {
            int h = height / threads + (extraPix-- > 0 ? 1 : 0);
            sections[i] = new SubImage(start, width, h);
            start += width * h;
        }
    }

    /**
     * Notifies the SubImages that they should all update.
     * 
     * @param information
     *            Any class (i.e. anything that inherits from {@link Object}). Allows for calculators to
     *            define their own requirements
     */
    public void update(Object information)
    {
    	for (int i = 0; i < sections.length; i++)
    		sections[i].update(information);
    }

    /**
     * Collects the most recent pixel data
     * 
     * @return int[] of pixel values.
     */
    public int[] getImageData()
    {
        for (int i = 0; i < sections.length; i++)
        {
            int[] subPixels = sections[i].getData();
            System.arraycopy(subPixels, 0, pixels, sections[i].start, subPixels.length);
        }

        return pixels;
    }

    /**
     * Gets the update rate of the MultithreadedImage. The update rate is managed by a
     * {@link MultiSourceRateTracker}.
     *
     * @return update rate
     */
    public int getUpdateRate()
    {
        return rateTracker.getRPS();
    }

    /**
     * A SubImage is a nested class within {@link MultithreadedImage} used to keep track of individual
     * sections of the pixel data. Each SubImage has its own {@link Executor} defined by
     * {@link Executors#newSingleThreadExecutor()}. Each time
     * {@link SubImage#update(Object information)} is called, a new {@link PixelValueCalculator}
     * operation is added to the Executor queue.
     */
    private class SubImage
    {
        private final int                   start, width, height;
        private final BlockingQueue<Object> updateQueue = new ArrayBlockingQueue<>(3, true);
        private volatile int[]              currentData;
        private Semaphore semaphore = new Semaphore(0);

        /**
         * Instantiates new SubImage.
         * 
         * @param start
         *            index where this SubImage starts in the MultithreadedImage
         * @param width
         *            width (in pixels) of the SubImage to create
         * @param height
         *            height (in pixels) of the SubImage to create
         */
        SubImage(int start, int width, int height)
        {
            this.start = start;
            this.width = width;
            this.height = height;
            init();
        }

        private void init()
        {
            new Thread(this::run).start();
        }

        public void update(Object information)
        {
            updateQueue.offer(information);
        }
        
        private int[] getData()
        {
        	Utils.aquireSemaphore(semaphore);
        	
        	return currentData;
        }

        /**
         * Adds an update operation to the Executor queue
         * 
         * @param information
         *            Any class (i.e. anything that inherits from {@link Object}). Allows for calculators to
         *            define their own requirements
         */
        public void run()
        {
            while (true)
            {
                try
                {
                    currentData = calculator.calculate(updateQueue.take(), start, width, height);
                    semaphore.release();
                    rateTracker.tick();
                }
                catch (InterruptedException e)
                {
                }
            }
        }

        @Override
        public String toString()
        {
            return String.format("Start pixel:  %d \n%dx%d\n", start, width, height);
        }
    }

}
