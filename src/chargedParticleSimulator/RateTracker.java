package chargedParticleSimulator;

/**
 * This class is used for multithreaded tracking of rates per second.
 * 
 * Use is: every cycle you want to count towards the rate must call tick() to get the rate, call
 * getRPS()
 * 
 * @author Yoni Rubenstein
 */
public class RateTracker
{
    private volatile long  timeElapsed, currentSecondStartTime, lastTickTime;
    private volatile int   updates, currentSecondUpdates;
    protected volatile int rps;

    /**
     * Instantiates a new RateTracker and initializes variables
     */
    public RateTracker()
    {
        updates = 0;
        timeElapsed = 0L;
        currentSecondStartTime = lastTickTime = System.nanoTime();
    }

    /**
     * Updates variables
     */
    public void tick()
    {
        ++updates;
        timeElapsed += System.nanoTime() - lastTickTime;
        lastTickTime = System.nanoTime();
        ++currentSecondUpdates;
        if (lastTickTime - currentSecondStartTime >= 1000000000d)
        {
            rps = currentSecondUpdates;
            currentSecondUpdates = 0;
            currentSecondStartTime = lastTickTime;
        }
    }

    /**
     * @return rate
     */
    public int getRPS()
    {
        return rps;
    }

    /**
     * @return total updates
     */
    public int getTotalUpdateCount()
    {
        return updates;
    }

    /**
     * @return total elapsed time
     */
    public long getTotalElapsedTime()
    {
        return timeElapsed;
    }

}
