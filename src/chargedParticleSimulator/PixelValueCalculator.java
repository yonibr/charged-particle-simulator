package chargedParticleSimulator;

/**
 * Functional interface for performing calculations on pixel values.
 * 
 * @author Yoni Rubenstein
 */
@FunctionalInterface
public interface PixelValueCalculator
{
    /**
     * @param information
     *            Any class (i.e. anything that inherits from {@link Object}). Allows for implementing
     *            functions to define their own input requirements
     * @param start
     *            index of first pixel to operate on
     * @param width
     *            pixel width of segment of data to operate on, which is not necessarily the "actual"
     *            width of the pixel array.
     * @param height
     *            pixel height of segment of data to operate on, which is not necessarily the "actual"
     *            height of the pixel array.
     * @return An int array representing pixel colors
     */
    int[] calculate(Object information, int start, int width, int height);
}
