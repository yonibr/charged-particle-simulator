package chargedParticleSimulator;

import java.util.concurrent.Semaphore;

public final class Utils {

	public static void aquireSemaphore(Semaphore semaphore)
	{
		boolean aquired = false;
		do {
        	try {
				semaphore.acquire();
				aquired = true;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	} while (!aquired);
	}
}
