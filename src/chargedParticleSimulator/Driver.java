package chargedParticleSimulator;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 *
 * This class is the driver for a charged particle simulator.
 *
 * The Driver class is final and its constructor is private. Therefore, it can
 * neither be called externally nor subclassed.
 *
 * Contains main()
 *
 *
 * @author Yoni Rubenstein
 */
@SuppressWarnings("serial") // This class has no need to be serialized
final class Driver extends JPanel {
	static final int DEFAULT_IMAGE_THREADS = 5;

	private static Rectangle bounds = new Rectangle(0, 0, 600, 600);

	private static Simulation simulation;

	private final RateTracker fpsTracker, cpsTracker, fieldUpdateTracker;
	private final Worker worker;

	private Image fieldImage = null;
	private MultithreadedImage multithreadedImage;

	private Semaphore calculationLock = new Semaphore(1);
	private Semaphore renderSemaphore = new Semaphore(0);

	volatile int threads = DEFAULT_IMAGE_THREADS;

	/**
	 * Instantiates Driver, and begins the calculations and rendering.
	 *
	 * Since this constructor is private, it can only be called from within this
	 * class.
	 */
	private Driver() {

		// Initializes trackers
		fpsTracker = new RateTracker();
		cpsTracker = new RateTracker();
		fieldUpdateTracker = new RateTracker();

		setBounds(bounds);

		simulation = new Simulation(Simulation.DEFAULT_PARTICLE_COUNT, bounds);

		// Creates timer to call calculate() every 17ms (~60x/s)
		Timer calculationTimer = new Timer();
		calculationTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				calculate();
			}
		}, 10, 17);

		new Thread(() -> {
			while (true) {
				Utils.aquireSemaphore(renderSemaphore);
				repaint();
			}
		}).start();

		// Instantiate and start Worker, a subclass of SwingWorker
		worker = new Worker();
		worker.execute();

		// Alert Worker on window resizing
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				worker.resize(Driver.this.getWidth(), Driver.this.getHeight());
				bounds = simulation.bounds = Driver.this.getBounds();
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(fieldImage, 0, 0, null);
		drawBoundingRectangle(g2d);
		drawRates(g2d);
		fpsTracker.tick();
	}

	/**
	 * Draws rectangle border around the simulation window.
	 *
	 * @param g2d {@link Graphics2D} object cast from Driver's {@link Graphics}
	 *            context
	 */
	private void drawBoundingRectangle(Graphics2D g2d) {
		g2d.setStroke(new BasicStroke(20));
		g2d.setColor(Color.WHITE);
		g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	/**
	 * Draws different rates to top of simulation window.
	 *
	 * Current displayed rates are repaint framerate (caps at ~60/s), calculation
	 * rate (caps at ~60/s), field update rate, and field calculation rate.
	 *
	 * @param g2d {@link Graphics2D} object cast from Driver's {@link Graphics}
	 *            context
	 */
	private void drawRates(Graphics2D g2d) {
		g2d.setFont(new Font("Ariel", Font.BOLD, 12));
		g2d.setColor(Color.RED);
		g2d.drawString("FPS: " + fpsTracker.getRPS(), 10, 10);
		g2d.drawString("CPS: " + cpsTracker.getRPS(), 90, 10);
		g2d.drawString("FUPS: " + fieldUpdateTracker.getRPS(), 170, 10);
		g2d.drawString("FCPS: " + multithreadedImage.getUpdateRate(), 250, 10);
	}

	/**
	 * Calculates particle motion.
	 *
	 * Could be optimized slightly by sharing acceleration data between particles,
	 * but there's not much point since this isn't a bottleneck and readability
	 * would be sacrificed.
	 */
	private void calculate() {
		Utils.aquireSemaphore(calculationLock);
		simulation.calculate();
		calculationLock.release();
		cpsTracker.tick();
	}

	public void rebuildImage() {
		worker.rebuild.set(true);
	}

	public void newSimulation(int numParticles) {
		simulation = new Simulation(numParticles, bounds);
	}

	/**
	 * Application entry point
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame("Simulation");
		Driver driver = new Driver();
		frame.add(driver);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(bounds);

		JFrame settingsFrame = new JFrame("Settings");
		settingsFrame.add(new SimulationSettingsPanel(driver));
		settingsFrame.pack();
		frame.setVisible(true);
		settingsFrame.setVisible(true);
	}

	/**
	 * Worker is a subclass of {@link SwingWorker} which updates the image being
	 * drawn by Driver
	 */
	private class Worker extends SwingWorker<Void, Image> {
		// For thread safety
		AtomicBoolean rebuild;
		AtomicInteger width, height;

		/**
		 * Instantiates a new Worker
		 */
		public Worker() {
			rebuild = new AtomicBoolean(false);
			width = new AtomicInteger(bounds.width);
			height = new AtomicInteger(bounds.height);
		}

		@Override
		protected void process(List<Image> chunks) {
			for (Image bufferedImage : chunks) {
				fieldImage = bufferedImage;
			}
		}

		/**
		 * Tells worker to resize image.
		 *
		 * @param width  new image width
		 * @param height new image height
		 */
		void resize(int width, int height) {
			rebuild.set(true);
			this.width.set(width);
			this.height.set(height);
		}

		@Override
		protected Void doInBackground() throws Exception {
			// Instantiate the multithreaded image
			multithreadedImage = new MultithreadedImage(bounds.width, bounds.height, DEFAULT_IMAGE_THREADS,
					new FieldImageCalculator());
			BufferedImage bi = new BufferedImage(bounds.width, bounds.height, BufferedImage.TYPE_INT_ARGB);

			while (true) {
				int w = width.get();
				int h = height.get();

				// Handle resizing
				if (rebuild.get()) {
					multithreadedImage = new MultithreadedImage(w, h, threads, new FieldImageCalculator());
					bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
					rebuild.set(false);
				}

				// Tell image to update, then fetch pixel data
				Utils.aquireSemaphore(calculationLock);
				multithreadedImage.update(simulation.particles);
				int[] mem = multithreadedImage.getImageData();
				calculationLock.release();

				// Get BufferedImage from pixel data (int[])
				Graphics2D g2 = bi.createGraphics();
				Image img = createImage(new MemoryImageSource(w, h, mem, 0, w));
				g2.drawImage(img, 0, 0, null);
				simulation.drawParticles(g2);
				g2.dispose();
				publish(bi);
				renderSemaphore.release();

				/*
				 * N.B. This has nothing to do with the speed that the image itself is updating.
				 * Rather, it tracks how many times the image's pixel data is fetched and used
				 * to update the image being drawn.
				 */
				fieldUpdateTracker.tick();
			}
		}
	}
}
