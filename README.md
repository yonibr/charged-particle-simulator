# Charged Particle Simulator

This is an (Electrically) charged particle simulator. Particles have:

  - Mass
  - Charge (or neutral)
  - Velocity
  - Position

This simulator:

  - Utilizes multithreading for (reasonably) good performance
  - Can scale to faster hardware by adjusting sliders during runtime
  - Is well documented
  - Can be resized
  - Looks nice 😉

The purpose of this project is to challenge myself to optimize the code without using a GPU for calculations. It would be a relatively simple task to write a shader to do all of the necessary calculations in parallel, so maybe I'll add that option later to allow for more particles and rendering on computers with slow CPUs.

### Version
0.8.8

### Tech

Requires Java 8

### Installation

Either clone the repository and import into Eclipse (or whatever IDE you want), or download the uploaded jar.

### Todos

 - Optimize further
 - OpenGL shader version
 - Add mode for saving video instead of live rendering


License
----

MIT

Copyright (c) 2016 Yoni Rubenstein

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.